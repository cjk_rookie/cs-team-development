package com.csmall.mall.base;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * 分页查询参数
 */
@Data
public class PageTableRequest implements Serializable {

    private static final long serialVersionUID = 7328071045193618467L;
    private Integer page; //页数
    private Integer limit;  //多少条数据
    private Integer offset;  //开始查询的位置

    public void countOffset() {
		if (null == this.page || null == this.limit) {
            this.offset = 0;
            return;
        }
		this.offset = (this.page - 1) * this.limit;
    }

    @Override
    public String toString() {
        return "PageTableRequest{" +
                "page=" + page +
                ", limit=" + limit +
                ", offset=" + offset +
                '}';
    }
}
