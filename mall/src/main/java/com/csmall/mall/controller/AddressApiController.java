package com.csmall.mall.controller;

import com.csmall.mall.model.Address;
import com.csmall.mall.service.AddressService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("address")
public class AddressApiController {
    @Autowired
    private AddressService addressServer;

    @PostMapping("/selectaddress")
    @ResponseBody
    @ApiOperation("根据用户ID查询地址")
    @CrossOrigin
    public List<Address> SelectAddress(@RequestParam("userid") int userid){
        if (userid>0){
            return addressServer.SelectAddress(userid);
        }else {
            return null;
        }
    }
}
