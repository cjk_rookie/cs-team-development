package com.csmall.mall.controller;

import com.csmall.mall.base.Results;
import com.csmall.mall.model.Admin;
import com.csmall.mall.model.User;
import com.csmall.mall.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class AdminContrller {
    @Autowired
    AdminService adminService;

    @PostMapping("/user/login")
    @ResponseBody
    public Results<User> userLongin(HttpServletRequest request, HttpServletResponse response, Admin admin){
//        String username = admin.getName();
//        username = HtmlUtils.htmlEscape(username);


        Admin user = adminService.find(admin.getName(),admin.getPassword());
        request.getSession().setAttribute("USER",user);

        if (null == user){
            String message = "账号密码错误";
            return new Results(400,message);
        }else{

            return Results.success();
        }


    }

}
