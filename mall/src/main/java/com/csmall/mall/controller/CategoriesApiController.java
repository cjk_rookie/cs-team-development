package com.csmall.mall.controller;

import com.csmall.mall.base.Results;
import com.csmall.mall.model.Categories;
import com.csmall.mall.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("cate")
@Controller
public class CategoriesApiController {

    @Autowired
    private CategoriesService categoriesService;

    @RequestMapping("getAllCate")
    @ResponseBody
    public Results<Categories> getAllCate(){
        return categoriesService.getAllCate();
    }







}
