package com.csmall.mall.controller;

import com.csmall.mall.base.PageTableRequest;
import com.csmall.mall.base.Results;
import com.csmall.mall.dto.GoodsDeo;
import com.csmall.mall.dto.GoodsDto;
import com.csmall.mall.model.Goods;
import com.csmall.mall.service.GoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("goods")
@Api("商品控制器类")
public class GoodsApiController {

    @Autowired
    private GoodsService goodsService;

    @PostMapping("/listGoodsByState")
    @ResponseBody
    @ApiOperation("分页查询")
    @CrossOrigin
    public List<Goods> listGoodsByState(@RequestBody PageTableRequest request){
        request.countOffset();
        return goodsService.listGoodsByState(request.getOffset(),request.getLimit());
    }

    @GetMapping("/list")
    @ResponseBody
    @ApiOperation("分页查询")
    public Results<GoodsDeo> list(PageTableRequest request){
        request.countOffset();
        return goodsService.list(request.getOffset(),request.getLimit());
    }

    @RequestMapping("/listGoods")
    @ResponseBody
    @ApiOperation("展示所有商品")
    @CrossOrigin
    public List<Goods> listGoods(){
        return goodsService.listGoods();
    }

    @GetMapping("/list/{cid}")
    @ResponseBody
    @ApiOperation("分类展示商品")
    @CrossOrigin
    public List<Goods> list(@PathVariable("cid") int cid) throws Exception{
        if (0!=cid){
            return goodsService.findGoods(cid);
        }
        else {
            return null;
        }
    }

    @GetMapping("/listId/{gid}")
    @ResponseBody
    @ApiOperation("展示商品详细")
    @CrossOrigin
    public List<Goods> listid(@PathVariable("gid") int gid) throws Exception{
        if (0!=gid){
            return goodsService.findByGoods(gid);
        }
        else {
            return null;
        }
    }


    //搜索功能
    @GetMapping("/search/{sname}")
    @ResponseBody
    @CrossOrigin
    public List<Goods> search(@PathVariable("sname") String sname) throws Exception{
        if (null!=sname){
            System.out.println("参数为"+sname);
            return goodsService.searchGoods(sname);
        }
        else {
            return null;
        }
    }

    //针对layui的搜索接口
    @GetMapping("/findGoodInfoByGoodsName")
    @ResponseBody
    @ApiOperation("模糊查询商品信息")
    public Results<GoodsDeo> findGoodInfoByGoodsName(PageTableRequest pageTableRequest, String goodsname){
        pageTableRequest.countOffset();
        return goodsService.findGoodInfoByGoodsName(goodsname,pageTableRequest.getOffset(),pageTableRequest.getLimit());

    }


    //增加商品
    @PostMapping("/insertGoods")
    @ResponseBody
    @ApiOperation("增加商品接口")
    public Results<GoodsDto> insertGoods(GoodsDto goodsDto){

        if(goodsDto != null){
            int count = goodsService.insertGoods(goodsDto);
            if(count>0){
                return Results.success("增加成功");
            }else {
                return Results.failure(500,"增加失败");
            }
        }else{
            return Results.failure(500,"增加失败");
        }

    }

    //修改
    @RequestMapping("/editGoods")
    @ApiOperation("接口")
    public String editGoods(Model model, Goods goods){
        model.addAttribute(goodsService.getGoodsByGoodid(goods.getGoosid()));
        return "goods/goods-edit";
    }

    //查找类别
    @PostMapping("/selectByGoodsId")
    @ResponseBody
    @ApiOperation("接口")
    public Results<Goods> selectByGoodsId(Integer goodsid){

        return Results.success(goodsService.getGoodsByGoodid(goodsid));
    }

    String pattern = "yyyy-MM-dd";
    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request){
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat(pattern),true));

    }

    //修改操作
    @PostMapping("/updateGoods")
    @ResponseBody
    @ApiOperation("修改操作接口")
    public Results<Goods> updateGoods(GoodsDto goodsDto){
        System.out.println("0000000000000--------10010"+goodsDto.getSheiftime());
        System.out.println("0000000000000"+goodsDto.getGoodsname());
        int count = goodsService.updateGoods(goodsDto);
        if (count>0){
            return Results.success("修改成功");
        }else {
            return Results.failure(500,"修改失败");
        }
//        return Results.failure(500,"修改失败");
    }

    //删除商品
    @RequestMapping("/deleteGoods")
    @ResponseBody
    @ApiOperation("删除商品接口")
    public Results deleteGoods(int goodsid){
        int count = goodsService.deleteGoods(goodsid);
        if (count>0){
            return Results.success();
        }else {
            return Results.failure();
        }
    }




}
