package com.csmall.mall.controller;

import com.csmall.mall.base.Results;
import com.csmall.mall.dto.UserDto;
import com.csmall.mall.model.Admin;
import com.csmall.mall.model.User;
import com.csmall.mall.service.AdminService;
import com.csmall.mall.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Controller
public class LoginApiController {

    @Autowired
    UserService userService;



    @CrossOrigin
    @PostMapping(value = "/api/login")
    @ResponseBody
    public Results<UserDto> login(@RequestBody User requestUser){
        String username = requestUser.getUsername();
        username = HtmlUtils.htmlEscape(username);

        User user = userService.find(username,requestUser.getPassword());

        if (null == user){
            String message = "账号密码错误";
            return new Results(400,message);
        }else{
            UserDto userDto = new UserDto();
            userDto.setUserid(user.getUserid());
            userDto.setUsername(user.getUsername());
            return new Results().success(userDto);
        }
    }

    @GetMapping("/cofig/login")
    public String loginConfig(){
        return "login";
    }



}
