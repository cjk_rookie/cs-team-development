package com.csmall.mall.controller;

import com.csmall.mall.model.Address;
import com.csmall.mall.service.AddressService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("address")
public class MyMallApiController {
    @Autowired
    private AddressService addressService;

    @RequestMapping("/listAddress")
    @ResponseBody
    @CrossOrigin
    public List<Address> listAddress(){
        return addressService.listAddress();
    }

    @PostMapping("/insertAddress")
    @ResponseBody
    @CrossOrigin
    public int insertAddress(@RequestBody Address address) {
        if (address != null) {
            if (address.getUserid() > 0) {
                return addressService.InsertAddress(address);
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }


    /**
     * 根据ID来删除表中的数据
     * return 0 或 -1 表示失败
     * */

    @GetMapping("/deleteAddress/{addressid}")
    @ResponseBody
    @ApiOperation("根据ID来删除表中的数据")
    @CrossOrigin
    public int deleteAddress(@PathVariable int addressid){
        if (addressid>0){
            return addressService.deleteAddressByAddressId(addressid);
        }else {
            return -1;
        }
    }




    @PostMapping("/updateAddress")
    @ResponseBody
    @ApiOperation("根据ID来修改表）")
    @CrossOrigin
    public int updateAddress(@RequestBody Address address){
        if(address.getAddressid()>0){
            return addressService.updateAddressByaddressId(address);
        }else {
            return -1;
        }

    }


    @GetMapping("/selectByAddressid")
    @ResponseBody
    @CrossOrigin
    public List<Address> selectByAddressid(Integer adressid){
        return addressService.selectByAddressid(adressid);
    }



}
