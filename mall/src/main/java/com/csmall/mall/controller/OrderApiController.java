package com.csmall.mall.controller;

import com.csmall.mall.dto.OrderformDto;
import com.csmall.mall.service.OrderFormService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("order")
public class OrderApiController {
    @Autowired
    private OrderFormService orderformServer;

    @PostMapping("/insertorder")
    @ResponseBody
    @ApiOperation("插入订单表")
    @CrossOrigin
    public int insertorder(@RequestBody OrderformDto orderform){
        return orderformServer.Insertorder(orderform);

    }

    @PostMapping("/pay")
    @ResponseBody
    @ApiOperation("更改订单表")
    @CrossOrigin
    public void updata(@RequestParam Integer userid){
        orderformServer.updatestutas(userid);
    }
}
