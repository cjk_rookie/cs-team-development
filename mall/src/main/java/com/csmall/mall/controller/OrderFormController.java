package com.csmall.mall.controller;

import com.csmall.mall.base.PageTableRequest;
import com.csmall.mall.base.Results;
import com.csmall.mall.dto.OrderDto;
import com.csmall.mall.service.OrderFormService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("order")
@Controller
public class OrderFormController {

    @Autowired
    private OrderFormService orderFormService;



    //查看所有的订单
    @GetMapping("listOrder")
    @ResponseBody
    @ApiOperation("查看所有订单")
    public Results<OrderDto> listOrder(PageTableRequest request){
        request.countOffset();
        return orderFormService.listOrder(request.getOffset(),request.getLimit());


    }


    @PostMapping("/selectOrder")
    @ResponseBody
    @CrossOrigin
    public List<OrderDto> selectOrder(){
        return orderFormService.selectOrder();
    }

    @PostMapping("/selectOrderid")
    @ResponseBody
    @CrossOrigin
    public List<OrderDto> selectOrderid(Integer orderformid){
        return orderFormService.selectOrderid(orderformid);
    }


}
