package com.csmall.mall.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.csmall.mall.service.OrderFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Controller
public class PayAPIController {
    private final String APP_ID = "2016110100783198";
    private final String APP_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCz38nlrxk+3GyPbp23EGbn16KjBN9U6UGQlMoYxYJQXHdol7sYGmcsYN2YyKGIRw17VvuGJHJL96Rh8dzhxg3GyoWVdx1om+xFfqIANmKjI0kympYBp55f/FUGp651NUf67WcU/GIsq+ybDIeDRu5klm+Ae4xkudBwfpG2lA5kvPjMFNm9aHaASHB2hCA9hp69c1vv6OEqpxepEhq8szcIqi4jQFAf6eLKaPfnuSU944d7g8IfLPBPXddF+aHiLxK6o4YM8SX98WP+c7IJWwMCNpa3Kfni13ZRrWE3K2o3T6TLMZ2CfwUphujmwKhoNkdllE4iqdxQ+urApPnIVkYXAgMBAAECggEAG0Mm6Z4BWIq2FYMxekaNIkSHjOe3Jbp4yn4Sp3CAEMgC/SJKJBwUWSCRVY/LE8mZHIGRe8JYZHUzP/kVfk6zVdbqKj40gd9oDxxjV17m0k0N0hKl+7g5A9lN1cViZrddooWK4x5hjt277cAXZelBRpOacMHJBJc5JWO5mA+WouPUJH/OrPbyEUZ3FQDCuqvWoifgW/gnWMMJ7wp22LeDy2xGwuZZkP9/5fSpr+9W1gaYd0hJFgRtYGSIBKMcYgdOr99rgac1c7XZF9el6gOzMLaXLoICbqz3uhKkmw6B1DYLSXOz1ENoCcaFZlPrT1c/6MZZzjWssffZxb4blesOMQKBgQDm7glOGWKk9IKS75seHkd1risqGDN2TKYjTQVIinVqAC9d4TO9wfz33RBHUpsItxOVOhMz7vOX7abHTcK6/Dd+32vsiTM8R8jOz24IdE+Roy2I4sIVSF5EIHmrXB4EtiuTYFsmEg77mrElm8XQ1cs8gEGrKNhQ5OR0+Sljz15aHwKBgQDHZtJtwBK61aZCQqR1uibSuKw0huXp2fKOMdL9dsL7HcQhzFepS608bzNUHtibwcTgjY2DsOtmBSRajSbEiiw9DJ7jBDNA00YfkK50NzdhR0dS0hTURYrJ1mvMzRSkPg7RijQQ0DPtfKAFtoVhKmKbw2zOhGlfuJElCz4Bpm2FCQKBgQCYYk8FmD69txNKvo3PbA+fmhSlrF6ZG68t8C8iVStxjTm0a1znRNEXBmsthVcEgzqK6E2voWhgwtOhBLERDknYkLn41fhXOM9LmRl0vrua0ODFSUzlGmIJ9pwS9bBhT4ylrZm4H4Z/hO6McKQyUEHSZJfwx/0mNESE473qPPZ35QKBgQCVeEfoqP/hOWHFbXsmHIIZlwDO5wfVAeDGIicMhEeoQSnSRAC6ENYs5RZOykiIiPlxcepQewAZm2Ui04fp78E1ig5YEb/xqe14n9WFnBZRP0MBnCSs8ewVXDu2a0w7yaEGBVj9+XPTY4X4glb8/O6p4BXhwHPUot96KzExv3V9MQKBgGydid30zffD0sxuF/uATKZemNC696Wwr9BuVs3AC8+NBBt62udXIjtG4uroZ/7zEAZvR1cr/TRYtBTDogQ5nm5GTCWADHYwuQkdprxKePYOwDwRpNmHks2xrpNeGJlEbEe5vsW9JdgFIuaCmbtoktuVdmZ0njTUDtB+meS6qFN0";
    private final String CHARSET = "UTF-8";
    private final String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkirX6GIReNJJ+kWZDBwtUstN0islDTGfgguEozD6AtasOcgRaOmfD80RbcvFXBuPTlWQZwR60xRCp+bs9azbN63OesND3q8z0h88bOlXHW0NW5AKntgbLjYpIaoae/1XFRzJzIOJ7hWFGHIv3tbGpIdhMNrEbbiuSZ/H3KCyVNxFXjw+ETVZbGHe5dYjzYE0AAoxgkcht7GkGxYDql3JXcOWFOhgEPGBAPqcgmlZu8zK7jT1ZmadShRk4b4iG0VVX3ac20AwT6Kt/Zx6Ng1DZ9g+/89D5EMG7Z51MZveaIbyv6RwQzb2QYo6YkcW8eJdI9n/kR6yLPQ/bI0G3e4pkQIDAQAB";
    //这是沙箱接口路径,正式路径为https://openapi.alipay.com/gateway.do
    private final String GATEWAY_URL ="https://openapi.alipaydev.com/gateway.do";
    private final String FORMAT = "JSON";
    //签名方式
    private final String SIGN_TYPE = "RSA2";
    //支付宝异步通知路径,付款完毕后会异步调用本项目的方法,必须为公网地址
    private final String NOTIFY_URL = "http://127.0.0.1/notifyUrl";
//    private final String NOTIFY_URL = "http://localhost:8081/returnUrl";
    //支付宝同步通知路径,也就是当付款完毕后跳转本项目的页面,可以不是公网地址
    private final String RETURN_URL = "http://localhost:8080/pay";
    //    private final String RETURN_URL = "http://127.0.0.1/returnUrl";
    public int userid;
    @Autowired
    private OrderFormService orderformService;
    @PostMapping("/alipay")
    @CrossOrigin
    public void alipay(HttpServletResponse httpResponse,@RequestParam Integer totals,@RequestParam Integer userid) throws IOException {

        System.out.println("商品名称为"+totals);
        this.userid = userid;
        Random r=new Random();
        //实例化客户端,填入所需参数
        AlipayClient alipayClient = new DefaultAlipayClient(GATEWAY_URL, APP_ID, APP_PRIVATE_KEY, FORMAT, CHARSET, ALIPAY_PUBLIC_KEY, SIGN_TYPE);
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
        //在公共参数中设置回跳和通知地址
        request.setReturnUrl(RETURN_URL);
        request.setNotifyUrl(NOTIFY_URL);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        //生成随机Id
        String out_trade_no = UUID.randomUUID().toString();
        //付款金额，必填
//        String total_amount =Integer.toString(11111);
        String total_amount =Integer.toString(totals);
        //订单名称，必填
        String subject ="*************";
        //商品描述，可空
        String body = "**************************";
        request.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        String form = "";
        try {
            form = alipayClient.pageExecute(request).getBody(); // 调用SDK生成表单
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        httpResponse.setContentType("text/html;charset=" + CHARSET);
        httpResponse.getWriter().write(form);// 直接将完整的表单html输出到页面
        httpResponse.getWriter().flush();
        httpResponse.getWriter().close();
    }
    @RequestMapping(value = "/returnUrl", method = RequestMethod.GET)
    public String returnUrl(HttpServletRequest request, HttpServletResponse response)
            throws IOException, AlipayApiException {
        System.out.println("=================================同步回调=====================================");

        // 获取支付宝GET过来反馈信息
        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            // 乱码解决，这段代码在出现乱码时使用
            valueStr = new String(valueStr.getBytes("utf-8"), "utf-8");
            params.put(name, valueStr);
        }

        System.out.println(params);//查看参数都有哪些
        boolean signVerified = AlipaySignature.rsaCheckV1(params, ALIPAY_PUBLIC_KEY, CHARSET, SIGN_TYPE); // 调用SDK验证签名
        //验证签名通过
        if(signVerified){
            // 商户订单号
            String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");

            // 支付宝交易号
            String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");

            // 付款金额
            String total_amount = new String(request.getParameter("total_amount").getBytes("ISO-8859-1"), "UTF-8");

            System.out.println("商户订单号="+out_trade_no);
            System.out.println("支付宝交易号="+trade_no);
            System.out.println("付款金额="+total_amount);

            //支付成功，修复支付状态
            orderformService.updatestutas(userid);
            System.out.println("1111111111-----------------");
            return "ok";//跳转付款成功页面
        }else{
            orderformService.updates(userid);
            System.out.println("222222222222-----------------");
            return "no";//跳转付款失败页面
        }

    }
}