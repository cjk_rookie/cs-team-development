package com.csmall.mall.controller;

import com.csmall.mall.base.Results;
import com.csmall.mall.model.User;
import com.csmall.mall.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;

@Controller
public class RegisterApiController {
    @Autowired
    UserService userService;

   /* @CrossOrigin
    @RequestMapping(value = "/api/reg",method=RequestMethod.GET)
    @ResponseBody
    public String toRegister(Model model){
        model.addAttribute("user",new User());
        return "register";
    }
    @RequestMapping(value = "/register",method=RequestMethod.POST)
    public ModelAndView register(@ModelAttribute(value = "user")) {
        return new ModelAndView("register");
    }*/
    @CrossOrigin
    @PostMapping("api/register")
    @ResponseBody
    public Results Register(@RequestBody User user){
        String username = user.getUsername();
        username = HtmlUtils.htmlEscape(username);
        user.setUsername(username);
        String password = user.getPassword();

        boolean isExist = userService.isExist(username);
        if(isExist){
            return new Results(500,"failure");
        }
        user.setPassword(password);
        userService.addUser(user);
        return new Results(200,"success");
    }
}
