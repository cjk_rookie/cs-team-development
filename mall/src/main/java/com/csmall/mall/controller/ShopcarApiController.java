package com.csmall.mall.controller;

import com.csmall.mall.dto.ShopCarDto;
import com.csmall.mall.model.Goods;
import com.csmall.mall.model.Shopcar;
import com.csmall.mall.model.User;
import com.csmall.mall.service.ShopcarServer;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("shopcar")
public class ShopcarApiController {
    @Autowired
    private ShopcarServer shopcarServer;

    @RequestMapping("/listShopcar")
    @ResponseBody
    @CrossOrigin
    public List<Shopcar> listShopcar(){
        return shopcarServer.listShopcar();
    }


    @PostMapping("/insertShopcar")
    @ResponseBody
    @CrossOrigin
    public int insertShopcar(@RequestBody Shopcar shopcar){
        System.out.println("颜色"+shopcar.getColor());
        if (shopcar !=null){
            if((shopcar.getStyle() != null||shopcar.getColor() != null)&&shopcar.getGoodsid()>0&&shopcar.getUserid()>0&&shopcar.getNum()>0){
                return shopcarServer.InsertShopcar(shopcar);

            }else {
                return -1;
            }
        }else {
            return -1;
        }



    }

    /**
     * 根据用户的ID返回所需的数据
     * */
    @GetMapping("/selectGoodsByUserId/{userId}")
    @ResponseBody
    @ApiOperation("根据用户的ID返回商品列表")
    @CrossOrigin
    public List<ShopCarDto> selectGoodsByUserId(@PathVariable int userId){
        if (userId<=0){
            return null;
        }else {
            return shopcarServer.selectGoodsByUserId(userId);
        }
    }

    /**
     * 修改订单，根据购物车ID来修改表中的num(数量），color(颜色），style(样式，如衣服的XL尺码）
     * 参数：
     *int num
     * String color
     * String style
     *
     * return -1表示修改失败
     * */
    @PostMapping("/updateShopCar")
    @ResponseBody
    @ApiOperation("根据购物车ID来修改表中的num(数量），color(颜色），style(样式，如衣服的XL尺码）")
    @CrossOrigin
    public int updateShopCarByUserIdAndGoodsId(@RequestBody Shopcar shopcar){
        if(shopcar.getId()>0){
            return shopcarServer.updateShopCarByUserIdAndGoodsId(shopcar);
        }else {
            return -1;
        }

    }


    /**
     * 根据购物车ID来删除购物车表中的数据
     *
     * return 0 或 -1 表示失败
     * */

//    @GetMapping("/deleteShopCar/{Id}")
//    @ResponseBody
//    @ApiOperation("根据购物车ID来删除表中的数据")
//    @CrossOrigin
//    public int deleteShopCar(@PathVariable int Id){
//        if (Id>0){
//            return shopcarServer.deleteShopCarByUserIdAndGoodsId(Id);
//        }else {
//            return -1;
//        }
//    }
    @PostMapping("/deleteShopCar")
    @ResponseBody
    @ApiOperation("根据购物车ID来删除表中的数据")
    @CrossOrigin
    public int deleteShopCar(@RequestParam("id") int Id){
        if (Id>0){
            return shopcarServer.deleteShopCar(Id);
        }else {
            return -1;
        }
    }

    @GetMapping("/setstuta/{userId}")
    @ResponseBody
    @ApiOperation("刷新更改状态")
    @CrossOrigin
    public int setstuta(@PathVariable int userId){
        if (userId<=0){
            return -1;
        }else {
            return shopcarServer.setstuta(userId);
        }
    }

    @GetMapping("/delshopcar")
    @ResponseBody
    @ApiOperation("删除状态为1")
    @CrossOrigin
    public void delshopcar(){
        shopcarServer.delshopcar();
    }


    @PostMapping("/updatestuta")
    @ResponseBody
    @ApiOperation("结算更改购物车状态为1")
    @CrossOrigin
    public int updatestuta(@RequestParam("id") int Id){
        if (Id>0){
            return shopcarServer.updatestuta(Id);
        }else {
            return -1;
        }
    }

    @PostMapping("/upstuta")
    @ResponseBody
    @ApiOperation("结算更改购物车状态为0")
    @CrossOrigin
    public int upstuta(@RequestParam("id") int Id){
        if (Id>0){
            return shopcarServer.upstuta(Id);
        }else {
            return -1;
        }
    }

    @PostMapping("/updatenum")
    @ResponseBody
    @ApiOperation("根据购物车ID更改表中的数量")
    @CrossOrigin
    public int updatenum(@RequestParam("id") int Id,@RequestParam("num") int num){
        if (Id>0){
            return shopcarServer.updatenum(Id,num);
        }else {
            return -1;
        }
    }







}
