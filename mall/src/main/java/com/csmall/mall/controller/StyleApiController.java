package com.csmall.mall.controller;

import com.csmall.mall.base.Results;
import com.csmall.mall.model.Style;
import com.csmall.mall.service.StyleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("styles")
@Api("商品样式控制器类")
public class StyleApiController {

    @Autowired
    private StyleService styleService;

    @PostMapping("listStyle")
    @ResponseBody
    @ApiOperation("根据商品ID返回样式列表")
    @CrossOrigin
    public List<Style> listStyle(@RequestParam int goodId){
        if (goodId >0){
            return styleService.listSytle(goodId);
        }else {
            return null;
        }
    }



}
