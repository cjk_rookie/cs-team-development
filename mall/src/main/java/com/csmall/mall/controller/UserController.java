package com.csmall.mall.controller;

import com.csmall.mall.base.PageTableRequest;
import com.csmall.mall.base.Results;
import com.csmall.mall.model.User;
import com.csmall.mall.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;




    @RequestMapping("/listUser")
    @ResponseBody
    public List<User> listUser(){
        return userService.listUser();
    }


    @RequestMapping("/list")
    @ResponseBody
    @ApiOperation("分页查询数据")
    public Results<User> list(PageTableRequest request){
        request.countOffset();
        return userService.list(request.getOffset(),request.getLimit());
    }

    @GetMapping("/editUser")
    @ApiOperation("根据ID查询用户信息")
    public String editUser(Model model, Integer userid){
        model.addAttribute(userService.editUserById(userid));
        return "user/user-edit";
    }

    @PostMapping("/editUser")
    @ResponseBody
    @ApiOperation("修改用户")
    public Results<User> updateUser(User user){
        int count = 0;
        if (user != null){
            count = userService.updateUser(user);
            System.out.println("count"+count);
            if (count>0){
                return Results.success("修改成功");
            }else {
                return Results.failure(500,"修改失败");
            }
        }else {

            return Results.failure(500,"修改失败");
        }
    }

    @RequestMapping("/add")
    @ApiOperation("增加用户界面")
    public String add(){
        return "user/user-add";
    }

    @PostMapping("/addUser")
    @ApiOperation("增加用户")
    @ResponseBody
    public Results<User> addUser(User user){
        int count = 0;

        if (user != null){
            User user1 = userService.getUserByname(user.getUsername());
            if (user1 != null){
                return Results.failure(500,"用户名已存在，请使用别的名称");

            }
            count = userService.addUserByLayui(user);
            if (count>0){
                return Results.success("增加成功");
            }else {
                return Results.failure(500,"增加失败");
            }
        }else {
            return Results.failure(500,"增加失败");
        }
    }

    //模糊查询
    @RequestMapping("/findUserByUsername")
    @ResponseBody
    @ApiOperation("根据用户名查找用户")
    public Results<User> findUserByUsername(PageTableRequest request,String username){
        request.countOffset();
        return userService.findUserByUsername(username,request.getOffset(),request.getLimit());
    }


    //删除用户
    @GetMapping("deleteUserByUserId")
    @ResponseBody
    @ApiOperation("根据用户ID删除用户")
    public Results<User> deleteUserByUserId(Integer userid){
        int count = 0;
        if (userid != null){
            count = userService.deleteUserByUserid(userid);
            if (count>0){
                return Results.success();
            }else {
               return Results.failure();
            }
        }else {
            return Results.failure();
        }
    }





}
