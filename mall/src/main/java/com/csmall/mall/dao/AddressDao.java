package com.csmall.mall.dao;

import com.csmall.mall.dto.AddressDto;
import com.csmall.mall.dto.ShopCarDto;
import com.csmall.mall.model.Address;
import com.csmall.mall.model.Shopcar;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AddressDao {

    @Select("select * from address where userid = #{userid}")
    List<Address> SelectAddress(int userid);

    @Select("select * from address")
    List<Address> listAddress();

    @Insert("insert into address(userid,consignee,aphone,province,city,county,address) values(#{userid},#{consignee},#{aphone},#{province},#{city},#{county},#{address})")
    @Options(useGeneratedKeys = true, keyProperty = "addressid")
    int InsertAddress(Address address);


    @Delete("delete from address where addressid = #{addressid}")
    int deleteAddress(@Param("addressid") int addressid);

    @Update("update address set userid = #{userid},consignee= #{consignee},aphone=#{aphone},province=#{province},city=#{city},county=#{county},address=#{address} where addressid=#{addressid}")
    int updateAddressByaddressId(Address address);

    @Select("select * from address where addressid = #{adressid}")
    List<Address> selectByAddressid(Integer adressid);
}
