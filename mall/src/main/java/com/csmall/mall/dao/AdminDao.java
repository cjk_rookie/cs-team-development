package com.csmall.mall.dao;

import com.csmall.mall.model.Admin;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.data.jpa.repository.JpaRepository;

@Mapper
public interface AdminDao {


    @Select("select * from `admin` where `name` = #{username} and `password` = #{password}")
    Admin findAdminByUsername(@Param("username") String username,@Param("password") String password);
}
