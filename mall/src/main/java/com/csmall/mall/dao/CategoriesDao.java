package com.csmall.mall.dao;

import com.csmall.mall.model.Categories;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CategoriesDao {

    @Select("select * from categories")
    List<Categories> getAllCate();
}
