package com.csmall.mall.dao;


import com.csmall.mall.base.Results;
import com.csmall.mall.dto.GoodsDeo;
import com.csmall.mall.dto.GoodsDto;
import com.csmall.mall.model.Goods;
import org.apache.ibatis.annotations.*;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Mapper
public interface GoodsDao{

    //分页查询商品
    @Select("select * from goods t order by t.goosid limit #{startPosition},#{limit}")
    List<Goods> listGoodsByStart(@Param("startPosition")Integer startPosition, @Param("limit")Integer limit);

    //分页查询商品
    @Select("select g.*,c.cname from goods g  inner join categories c on g.categoriesid=c.categoriesid order by g.goosid limit #{startPosition},#{limit}")
    List<GoodsDeo> listGoodsByGoods(@Param("startPosition")Integer startPosition, @Param("limit")Integer limit);


    //统计商品表中的总数
    @Select("select count(*) from goods")
    Long countAllGoods();


    @Select("select * from goods")
    List<Goods> listGoods();

    @Select("select * from goods where categoriesid=#{cid}")
    List<Goods> findGoods(int cid);

    @Select("select * from goods where goosid=#{gid}")
    List<Goods> findByGoods(int gid);

    @Select("select * from goods where goodsname like '%${goodsname}%'")
    List<Goods> searchGoods(String goodsname);

    @Delete("delete from goods where goosid = #{goodsid}")
    int deleteGoods(int goodsid);

    @Select("select * from goods where goosid = #{goodsid}")
    Goods getGoodsByGoodid(int goodsid);

    int updateGoods(GoodsDto goods);


    @Select("select count(*) from goods where goodsname like '%${goodsname}%'")
    Long getGoodsByGoodname(@Param("goodsname") String goodsname);

    @Select("select g.*,c.cname from goods g inner join categories c on g.categoriesid=c.categoriesid where g.goodsname like '%${goodsname}%' limit #{startPosition},#{limit}")
    List<GoodsDeo> findGoodInfoByGoodsName(@Param("goodsname") String goodsname,@Param("startPosition") Integer startPosition,@Param("limit") Integer limit);

    @Insert("insert into goods(goodsname,goodsprice,sheiftime,gimages,categoriesid,introduction,invertory) values(#{goodsname},#{goodsprice},#{sheiftime},#{gimages},#{categoriesid},#{introduction},#{invertory})")
    @Options(useGeneratedKeys = true, keyProperty = "goosid")
    int insertGoods(GoodsDto goodsDto);
}
