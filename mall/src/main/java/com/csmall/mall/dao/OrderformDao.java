package com.csmall.mall.dao;


import com.csmall.mall.base.Results;
import com.csmall.mall.dto.OrderDto;
import com.csmall.mall.dto.OrderformDto;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface OrderformDao {

    @Insert("insert into orderform(orderformid,userid,addressid,goodsid,goodsnum,total,stutas,btime) values(#{orderformid},#{userid},#{addressid},#{goodsid},#{goodsnum},#{total},#{stutas},#{btime})")
    int Insertorder(OrderformDto orderform);

    @Update("update orderform set stutas=1 where stutas=0 and userid=#{userid}")
    int updatestutas(int userid);

    @Update("update orderform set stutas=3 where stutas=0 and userid=#{userid}")
    int updates(int userid);


    @Select("select count(*) from orderform")
    Long countOrder();


    @Select("select o.*,a.*,g.goodsname,u.username from orderform o inner join address a on o.addressid = a.addressid inner join `user` u on u.userid = o.userid inner join goods g on g.goosid = o.goodsid limit #{startPage},#{limits}")
    List<OrderDto> listOrder(Integer startPage, Integer limits);


    @Select("select o.*,g.*,a.* from orderform o inner join goods g on g.goosid = o.goodsid inner join address a on a.addressid = o.addressid")
    List<OrderDto> selectOrder();

    @Select("select o.*,g.*,a.* from orderform o inner join goods g on g.goosid = o.goodsid inner join address a on a.addressid = o.addressid where orderformid = #{orderformid}")
    List<OrderDto> selectOrderid(Integer orderformid);


}
