package com.csmall.mall.dao;

import com.csmall.mall.dto.ShopCarDto;
import com.csmall.mall.model.Goods;
import com.csmall.mall.model.Shopcar;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ShopcarDao {
    @Select("select * from shopcar")
    List<Shopcar> listShopcar();

    @Insert("insert into shopcar(userid,goodsid,num,color,style) values(#{userid},#{goodsid},#{num},#{color},#{style})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int InsertShopcar(Shopcar shopcar);

    @Select("select g.*,b.* from shopcar b inner join goods g on b.goodsid = g.goosid where b.userid = #{userId}")
    List<ShopCarDto> selectGoodsByUserId(int userId);

    int updateShopCarByUserIdAndGoodsId(Shopcar shopcar);
    //删除购物车
    @Delete("delete from shopcar where id = #{id}")
    int deleteShopCar(@Param("id") int id);
    //删除状态为1
    @Delete("delete from shopcar where stuta = 1")
    int delshopcar();
    //修改状态为
    @Update("update shopcar set stuta=1 where id=#{id}")
    int updatestuta(@Param("id") int id);
    @Update("update shopcar set stuta=0 where id=#{id}")
    int upstuta(@Param("id") int id);
    @Update("update shopcar set stuta=0 where userid=#{id}")
    int setstuta(@Param("id") int id);
    //修改数量
    @Update("update shopcar set num=#{num} where id=#{id}")
    int updatenum(@Param("id") int id,@Param("num") int num);

}
