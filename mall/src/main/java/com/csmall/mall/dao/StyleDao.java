package com.csmall.mall.dao;

import com.csmall.mall.model.Style;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface StyleDao {

    @Select("select * from style where goodId = #{goodId}")
    List<Style> listStyle(int goodId);


}
