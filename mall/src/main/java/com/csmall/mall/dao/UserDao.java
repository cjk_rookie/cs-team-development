package com.csmall.mall.dao;

import com.csmall.mall.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.data.jpa.repository.JpaRepository;


@Mapper
public interface UserDao extends JpaRepository<User, Integer> {

    User findByUsername(String username);

    @Select("select * from user where username = #{username} and `password` = #{password}")
    User findAllByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    //@Insert("insert into user(username,password,phone,email) values('#{username}','#{password}','#{phone}','#{email}')")
    //User add(@Param("username") String username,@Param("password") String password,@Param("phone") String phone,@Param("email") String email);

    //@Insert("insert into user(username,password,phone,email,uimage,sex) values('#{username}','#{password}')")
    //User add(User user);

}
