package com.csmall.mall.deo;


import com.csmall.mall.model.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserDeo {



    @Select("select count(*) from user")
    Long countUser();

    @Select("select * from user limit #{startOffer},#{limits}")
    List<User> list(@Param("startOffer") Integer startOffer,@Param("limits") Integer limits);

    @Select("select * from user where userid = #{userid}")
    User getUserByUserId(Integer userid);


    int updateUser(User user);

    @Insert("insert into user(username,`password`,sex,email,phone) values(#{username},#{password},#{sex},#{email},#{phone})")
    @Options(useGeneratedKeys = true, keyProperty = "userid")
    int addUserByLayui(User user);

    @Select("select count(*) from user where username like '%${username}%'")
    Long findUser(String username);

    @Select("select * from user where username like '%${username}%' limit #{startPage},#{limits}")
    List<User> findUserByUsername(@Param("username") String username,@Param("startPage") Integer startPage,@Param("limits") Integer limits);

    @Delete("delete from user where userid = #{userid}")
    int deleteUserByUserId(Integer userid);

    @Select("select * from user where username = #{username}")
    User getUserByname(String username);
}
