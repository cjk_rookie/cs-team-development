package com.csmall.mall.dto;

import com.csmall.mall.model.Goods;

public class GoodsDeo extends Goods {
    private String cname;

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }
}
