package com.csmall.mall.dto;

import com.csmall.mall.model.Goods;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;


public class GoodsDto  {
    private int goosid;
    private int categoriesid;
    private String goodsname;
    private BigDecimal goodsprice;
    private Integer discount;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date sheiftime;
    private String gimages;
    private String introduction;
    private Integer invertory;

    public int getGoosid() {
        return goosid;
    }

    public void setGoosid(int goosid) {
        this.goosid = goosid;
    }

    public int getCategoriesid() {
        return categoriesid;
    }

    public void setCategoriesid(int categoriesid) {
        this.categoriesid = categoriesid;
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    public BigDecimal getGoodsprice() {
        return goodsprice;
    }

    public void setGoodsprice(BigDecimal goodsprice) {
        this.goodsprice = goodsprice;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Date getSheiftime() {
        return sheiftime;
    }

    public void setSheiftime(Date sheiftime) {
        this.sheiftime = sheiftime;
    }

    public String getGimages() {
        return gimages;
    }

    public void setGimages(String gimages) {
        this.gimages = gimages;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public Integer getInvertory() {
        return invertory;
    }

    public void setInvertory(Integer invertory) {
        this.invertory = invertory;
    }
}
