package com.csmall.mall.dto;

import com.csmall.mall.model.Orderform;

public class OrderDto extends Orderform {

    private String goodsname;
    private String gimages;

    private String username;
    private String aphone;
    private String province;
    private String city;
    private String county;
    private String address;

    public String getGimages() {
        return gimages;
    }

    public void setGimages(String gimages) {
        this.gimages = gimages;
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAphone() {
        return aphone;
    }

    public void setAphone(String aphone) {
        this.aphone = aphone;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
