package com.csmall.mall.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class OrderformDto {
    private int orderformid;
    private Integer goodsnum;
    private Integer total;
    private String stutas;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date btime;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date otiem;
    private int userid;
    private int goodsid;
    private int addressid;

    public int getOrderformid() {
        return orderformid;
    }

    public void setOrderformid(int orderformid) {
        this.orderformid = orderformid;
    }

    public Integer getGoodsnum() {
        return goodsnum;
    }

    public void setGoodsnum(Integer goodsnum) {
        this.goodsnum = goodsnum;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getStutas() {
        return stutas;
    }

    public void setStutas(String stutas) {
        this.stutas = stutas;
    }

    public Date getBtime() {
        return btime;
    }

    public void setBtime(Date btime) {
        this.btime = btime;
    }

    public Date getOtiem() {
        return otiem;
    }

    public void setOtiem(Date otiem) {
        this.otiem = otiem;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getGoodsid() {
        return goodsid;
    }

    public void setGoodsid(int goodsid) {
        this.goodsid = goodsid;
    }

    public int getAddressid() {
        return addressid;
    }

    public void setAddressid(int addressid) {
        this.addressid = addressid;
    }
}
