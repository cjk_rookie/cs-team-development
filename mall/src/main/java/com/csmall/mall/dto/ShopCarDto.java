package com.csmall.mall.dto;

import com.csmall.mall.model.Goods;
import lombok.Data;

@Data
public class ShopCarDto extends Goods {
    private Integer id;
    private Integer num;
    private String color;
    private String style;
    private String stuta;



}
