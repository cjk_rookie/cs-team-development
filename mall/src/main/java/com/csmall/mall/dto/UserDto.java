package com.csmall.mall.dto;

import lombok.Data;

@Data
public class UserDto {
    private int userid;
    private String username;

}
