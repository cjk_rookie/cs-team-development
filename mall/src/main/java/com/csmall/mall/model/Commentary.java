package com.csmall.mall.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Commentary {
    private int commentaryid;
    private String rank;
    private Timestamp time;
    private String content;

    @Id
    @Column(name = "commentaryid")
    public int getCommentaryid() {
        return commentaryid;
    }

    public void setCommentaryid(int commentaryid) {
        this.commentaryid = commentaryid;
    }

    @Basic
    @Column(name = "rank")
    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    @Basic
    @Column(name = "time")
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Commentary that = (Commentary) o;
        return commentaryid == that.commentaryid &&
                Objects.equals(rank, that.rank) &&
                Objects.equals(time, that.time) &&
                Objects.equals(content, that.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(commentaryid, rank, time, content);
    }
}
