package com.csmall.mall.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Goods {
    private int goosid;
    private int categoriesid;
    private String goodsname;
    private BigDecimal goodsprice;
    private Integer discount;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Timestamp sheiftime;
    private String gimages;
    private String introduction;
    private Integer invertory;

    public int getCategoriesid() {
        return categoriesid;
    }

    public void setCategoriesid(int categoriesid) {
        this.categoriesid = categoriesid;
    }

    @Id
    @Column(name = "goosid")
    public int getGoosid() {
        return goosid;
    }

    public void setGoosid(int goosid) {
        this.goosid = goosid;
    }

    @Basic
    @Column(name = "goodsname")
    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    @Basic
    @Column(name = "goodsprice")
    public BigDecimal getGoodsprice() {
        return goodsprice;
    }

    public void setGoodsprice(BigDecimal goodsprice) {
        this.goodsprice = goodsprice;
    }

    @Basic
    @Column(name = "discount")
    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    @Basic
    @Column(name = "sheiftime")
    public Timestamp getSheiftime() {
        return sheiftime;
    }

    public void setSheiftime(Timestamp sheiftime) {
        this.sheiftime = sheiftime;
    }

    @Basic
    @Column(name = "gimages")
    public String getGimages() {
        return gimages;
    }

    public void setGimages(String gimages) {
        this.gimages = gimages;
    }

    @Basic
    @Column(name = "introduction")
    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    @Basic
    @Column(name = "invertory")
    public Integer getInvertory() {
        return invertory;
    }

    public void setInvertory(Integer invertory) {
        this.invertory = invertory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Goods goods = (Goods) o;
        return goosid == goods.goosid &&
                Objects.equals(goodsname, goods.goodsname) &&
                Objects.equals(goodsprice, goods.goodsprice) &&
                Objects.equals(discount, goods.discount) &&
                Objects.equals(sheiftime, goods.sheiftime) &&
                Objects.equals(gimages, goods.gimages) &&
                Objects.equals(introduction, goods.introduction) &&
                Objects.equals(invertory, goods.invertory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(goosid, goodsname, goodsprice, discount, sheiftime, gimages, introduction, invertory);
    }
}
