package com.csmall.mall.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Orderform {
    private int orderformid;
    private Integer goodsnum;
    private Integer total;
    private String stutas;
    private Timestamp btime;
    private Timestamp otiem;

    @Id
    @Column(name = "orderformid")
    public int getOrderformid() {
        return orderformid;
    }

    public void setOrderformid(int orderformid) {
        this.orderformid = orderformid;
    }

    @Basic
    @Column(name = "goodsnum")
    public Integer getGoodsnum() {
        return goodsnum;
    }

    public void setGoodsnum(Integer goodsnum) {
        this.goodsnum = goodsnum;
    }

    @Basic
    @Column(name = "total")
    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @Basic
    @Column(name = "stutas")
    public String getStutas() {
        return stutas;
    }

    public void setStutas(String stutas) {
        this.stutas = stutas;
    }

    @Basic
    @Column(name = "btime")
    public Timestamp getBtime() {
        return btime;
    }

    public void setBtime(Timestamp btime) {
        this.btime = btime;
    }

    @Basic
    @Column(name = "otiem")
    public Timestamp getOtiem() {
        return otiem;
    }

    public void setOtiem(Timestamp otiem) {
        this.otiem = otiem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Orderform orderform = (Orderform) o;
        return orderformid == orderform.orderformid &&
                Objects.equals(goodsnum, orderform.goodsnum) &&
                Objects.equals(total, orderform.total) &&
                Objects.equals(stutas, orderform.stutas) &&
                Objects.equals(btime, orderform.btime) &&
                Objects.equals(otiem, orderform.otiem);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderformid, goodsnum, total, stutas, btime, otiem);
    }
}
