package com.csmall.mall.model;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
@Data
public class Shopcar {
    private int id;
    private int userid;
    private int goodsid;
    private Integer num;
    private String color;
    private String style;
    private Integer stuta;


    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getGoodsid() {
        return goodsid;
    }

    public void setGoodsid(int goodsid) {
        this.goodsid = goodsid;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "num")
    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    @Basic
    @Column(name = "color")
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Basic
    @Column(name = "style")
    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    @Basic
    @Column(name = "stuta")
    public Integer getStuta() {
        return stuta;
    }

    public void setStuta(Integer stuta) {
        this.stuta = stuta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shopcar shopcar = (Shopcar) o;
        return id == shopcar.id &&
                Objects.equals(num, shopcar.num) &&
                Objects.equals(color, shopcar.color) &&
                Objects.equals(style, shopcar.style) &&
                Objects.equals(stuta, shopcar.stuta);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, num, color, style, stuta);
    }
}
