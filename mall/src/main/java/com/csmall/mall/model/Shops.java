package com.csmall.mall.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Shops {
    private int shopsid;
    private String shopsname;
    private String simages;
    private String goodcomments;
    private String saddress;
    private String fansnum;

    @Id
    @Column(name = "shopsid")
    public int getShopsid() {
        return shopsid;
    }

    public void setShopsid(int shopsid) {
        this.shopsid = shopsid;
    }

    @Basic
    @Column(name = "shopsname")
    public String getShopsname() {
        return shopsname;
    }

    public void setShopsname(String shopsname) {
        this.shopsname = shopsname;
    }

    @Basic
    @Column(name = "simages")
    public String getSimages() {
        return simages;
    }

    public void setSimages(String simages) {
        this.simages = simages;
    }

    @Basic
    @Column(name = "goodcomments")
    public String getGoodcomments() {
        return goodcomments;
    }

    public void setGoodcomments(String goodcomments) {
        this.goodcomments = goodcomments;
    }

    @Basic
    @Column(name = "saddress")
    public String getSaddress() {
        return saddress;
    }

    public void setSaddress(String saddress) {
        this.saddress = saddress;
    }

    @Basic
    @Column(name = "fansnum")
    public String getFansnum() {
        return fansnum;
    }

    public void setFansnum(String fansnum) {
        this.fansnum = fansnum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shops shops = (Shops) o;
        return shopsid == shops.shopsid &&
                Objects.equals(shopsname, shops.shopsname) &&
                Objects.equals(simages, shops.simages) &&
                Objects.equals(goodcomments, shops.goodcomments) &&
                Objects.equals(saddress, shops.saddress) &&
                Objects.equals(fansnum, shops.fansnum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shopsid, shopsname, simages, goodcomments, saddress, fansnum);
    }
}
