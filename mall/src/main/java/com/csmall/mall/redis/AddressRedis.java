package com.csmall.mall.redis;

import com.csmall.mall.model.Address;
import org.springframework.stereotype.Repository;

@Repository
public class AddressRedis extends BaseRedis<Address>{
    private static final String REDIS_KEY = "com.dayup.seckil.redis.AddressRedis";

    @Override
    protected String getRedisKey() {
        return REDIS_KEY;
    }
}
