package com.csmall.mall.redis;

import com.csmall.mall.model.Goods;
import org.springframework.stereotype.Repository;

@Repository
public class GoodsRedis extends BaseRedis<Goods>{
    private static final String REDIS_KEY = "com.dayup.seckil.redis.GoodsRedis";

    @Override
    protected String getRedisKey() {
        return REDIS_KEY;
    }
}
