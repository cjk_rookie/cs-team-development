package com.csmall.mall.service;

import com.csmall.mall.model.Address;
import com.csmall.mall.model.Shopcar;

import java.util.List;

public interface AddressService {

    List<Address> SelectAddress(int userid);

    List<Address> listAddress();

    int InsertAddress(Address address);

    int deleteAddressByAddressId(int addressid);

    int updateAddressByaddressId(Address address);

    List<Address> selectByAddressid(Integer adressid);
}
