package com.csmall.mall.service;

import com.csmall.mall.model.Admin;

public interface AdminService {

   Admin find(String username, String password);
}
