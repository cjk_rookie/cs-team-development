package com.csmall.mall.service;

import com.csmall.mall.base.Results;
import com.csmall.mall.model.Categories;

public interface CategoriesService {


    Results<Categories> getAllCate();
}
