package com.csmall.mall.service;

import com.csmall.mall.base.Results;
import com.csmall.mall.dao.GoodsDao;
import com.csmall.mall.dto.GoodsDeo;
import com.csmall.mall.dto.GoodsDto;
import com.csmall.mall.model.Goods;

import java.util.List;

public interface GoodsService {

    //分页查询
    List<Goods> listGoodsByState(int start,int page);


    List<Goods> listGoods();

    List<Goods> findGoods(int cid);

    List<Goods> findByGoods(int gid);

    List<Goods> searchGoods(String goodsname);

    Results<GoodsDeo> list(Integer offset, Integer limit);

    int deleteGoods(int goodsid);

    Goods getGoodsByGoodid(int goosid);

    int updateGoods(GoodsDto goods);

    //模糊查询
    Results<GoodsDeo> findGoodInfoByGoodsName(String goodsname,Integer startPosition, Integer limit);

    int insertGoods(GoodsDto goodsDto);
}
