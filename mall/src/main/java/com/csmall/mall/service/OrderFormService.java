package com.csmall.mall.service;

import com.csmall.mall.base.Results;
import com.csmall.mall.dto.OrderDto;
import com.csmall.mall.dto.OrderformDto;
import com.csmall.mall.model.Orderform;

import java.util.List;

public interface OrderFormService {

    int Insertorder(OrderformDto orderform);

    int updatestutas(int userid);

    int updates(int userid);



    Results<OrderDto> listOrder(Integer startPage, Integer limits);

    List<OrderDto> selectOrder();

    List<OrderDto> selectOrderid(Integer orderformid);

}
