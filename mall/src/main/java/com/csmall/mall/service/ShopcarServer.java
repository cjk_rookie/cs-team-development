package com.csmall.mall.service;

import com.csmall.mall.dto.ShopCarDto;
import com.csmall.mall.model.Goods;
import com.csmall.mall.model.Shopcar;

import java.util.List;

public interface ShopcarServer {
    List<Shopcar> listShopcar();

    int InsertShopcar(Shopcar shopcar);

    List<ShopCarDto> selectGoodsByUserId(int userId);

    //修改订单
    int updateShopCarByUserIdAndGoodsId(Shopcar shopcar);

    int deleteShopCarByUserIdAndGoodsId(int id);

    int deleteShopCar(int id);

    int delshopcar();

    int updatestuta(int id);

    int upstuta(int id);

    int setstuta(int id);

    int updatenum(int id,int num);
}
