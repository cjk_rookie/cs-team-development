package com.csmall.mall.service;

import com.csmall.mall.model.Style;

import java.util.List;

public interface StyleService {

    List<Style> listSytle(int goodId);

}
