package com.csmall.mall.service;

import com.csmall.mall.base.Results;
import com.csmall.mall.model.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface UserService {

    List<User> listUser();
    
    
    //缪

    User getUserByusername(String username);
    boolean isExist(String username);
    void addUser(User user);

    User find(String username, String password);


    //5
    Results<User> list(Integer startPage,Integer limist);

    User editUserById(Integer userid);

    int updateUser(User user);

    int addUserByLayui(User user);

    Results<User> findUserByUsername(String username,Integer startPage,Integer limits);

    int deleteUserByUserid(Integer userid);

    User getUserByname(String username);

    // User add(User user);

}
