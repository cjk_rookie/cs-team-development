package com.csmall.mall.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;
import com.csmall.mall.dao.AddressDao;
import com.csmall.mall.model.Address;
import com.csmall.mall.model.Goods;
import com.csmall.mall.redis.AddressRedis;
import com.csmall.mall.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class AddressServiceImpl implements AddressService {
    @Autowired
    private AddressDao addressDao;

    @Override
    public List<Address> SelectAddress(int userId) {
        return addressDao.SelectAddress(userId);
    }

    @Override
    public List<Address> listAddress() {
        return addressDao.listAddress();
    }

    @Override
    public int InsertAddress(Address address) {
        return addressDao.InsertAddress(address);
    }

    @Override
    public int deleteAddressByAddressId(int addressid) {
        return addressDao.deleteAddress(addressid);
    }

    @Override
    public int updateAddressByaddressId(Address address) {
        return addressDao.updateAddressByaddressId(address);
    }

    @Override
    public List<Address> selectByAddressid(Integer adressid) {
        return addressDao.selectByAddressid(adressid);
    }
}
