package com.csmall.mall.service.impl;

import com.csmall.mall.dao.AdminDao;
import com.csmall.mall.model.Admin;
import com.csmall.mall.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminDao adminDao;

    @Override
    public Admin find(String username, String password) {
        return adminDao.findAdminByUsername(username,password);
    }
}
