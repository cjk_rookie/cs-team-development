package com.csmall.mall.service.impl;

import com.csmall.mall.base.Results;
import com.csmall.mall.dao.CategoriesDao;
import com.csmall.mall.model.Categories;
import com.csmall.mall.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CategoriesServiceImpl implements CategoriesService {

    @Autowired
    private CategoriesDao categoriesDao;


    @Override
    public Results<Categories> getAllCate() {
        return Results.success(50,categoriesDao.getAllCate());
    }
}
