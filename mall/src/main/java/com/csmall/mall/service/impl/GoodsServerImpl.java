package com.csmall.mall.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;
import com.csmall.mall.base.Results;
import com.csmall.mall.dao.GoodsDao;
import com.csmall.mall.dao.ShopcarDao;
import com.csmall.mall.dto.GoodsDeo;
import com.csmall.mall.dto.GoodsDto;
import com.csmall.mall.model.Goods;
import com.csmall.mall.redis.GoodsRedis;
import com.csmall.mall.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
@Service
@Transactional
public class GoodsServerImpl implements GoodsService{
    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private GoodsRedis goodsRedis;

    public static final String All_GOODS_REDIS="allGoodsRedis";

    @Override
    public List<Goods> listGoodsByState(int start, int page) {
        return goodsDao.listGoodsByStart(start,page);
    }

    @Override
    public List<Goods> listGoods() {
        List<Goods> goodsList = new ArrayList<>();
        //redis读取数据
        String goodsListString = (String)goodsRedis.getString(All_GOODS_REDIS);
        goodsList = JSON.parseArray(goodsListString,Goods.class);
        //MySQL读取数据
        if (StringUtils.isEmpty(goodsListString)){
            //读取数据
            goodsList=goodsDao.listGoods();
            String goodsLists = JSON.toJSONString(goodsList);
            goodsRedis.putString(All_GOODS_REDIS,goodsLists,-1);
        }
        return goodsList;
    }

    @Override
    public List<Goods> findGoods(int cid) {
        return  goodsDao.findGoods(cid);
    }

    @Override
    public List<Goods> findByGoods(int gid) { return  goodsDao.findByGoods(gid);
    }

    @Override
    public List<Goods> searchGoods(String goodsname) {return  goodsDao.searchGoods(goodsname);
    }

    @Override
    public Results<GoodsDeo> list(Integer offset, Integer limit) {
        return Results.success(goodsDao.countAllGoods().intValue(),goodsDao.listGoodsByGoods(offset,limit));
    }

    @Override
    public int deleteGoods(int goodsid) {
        return goodsDao.deleteGoods(goodsid);
    }

    @Override
    public Goods getGoodsByGoodid(int goosid) {
        return goodsDao.getGoodsByGoodid(goosid);
    }

    @Override
    public int updateGoods(GoodsDto goods) {
        return goodsDao.updateGoods(goods);
    }

    @Override
    public Results<GoodsDeo> findGoodInfoByGoodsName(String goodsname, Integer startPosition, Integer limit) {
        return Results.success(goodsDao.getGoodsByGoodname(goodsname).intValue(),goodsDao.findGoodInfoByGoodsName(goodsname,startPosition,limit));
    }

    @Override
    public int insertGoods(GoodsDto goodsDto) {
        return goodsDao.insertGoods(goodsDto);
    }


}
