package com.csmall.mall.service.impl;

import com.csmall.mall.base.Results;
import com.csmall.mall.dao.OrderformDao;
import com.csmall.mall.dto.OrderDto;
import com.csmall.mall.dto.OrderformDto;
import com.csmall.mall.model.Orderform;
import com.csmall.mall.service.OrderFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class OrderFormServiceImpl implements OrderFormService {

    @Autowired
    private OrderformDao orderformDao;

    @Override
    public int Insertorder(OrderformDto orderform){
        return orderformDao.Insertorder(orderform);
    }

    @Override
    public int updatestutas(int userid) {
        return orderformDao.updatestutas(userid);
    }

    @Override
    public int updates(int userid) {
        return orderformDao.updates(userid);
    }


    @Override
    public Results<OrderDto> listOrder(Integer startPage, Integer limits) {
        return Results.success(orderformDao.countOrder().intValue(),orderformDao.listOrder(startPage,limits));
    }

    @Override
    public List<OrderDto> selectOrder() {
        return orderformDao.selectOrder();
    }

    @Override
    public List<OrderDto> selectOrderid(Integer orderformid) {
        return orderformDao.selectOrderid(orderformid);
    }
}
