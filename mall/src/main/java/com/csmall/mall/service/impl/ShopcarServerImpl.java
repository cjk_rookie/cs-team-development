package com.csmall.mall.service.impl;

import com.csmall.mall.dao.ShopcarDao;
import com.csmall.mall.dto.ShopCarDto;
import com.csmall.mall.model.Goods;
import com.csmall.mall.model.Shopcar;
import com.csmall.mall.service.ShopcarServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ShopcarServerImpl implements ShopcarServer {
    @Autowired
    private ShopcarDao shopcardao;

     @Override
     public List<Shopcar> listShopcar(){return shopcardao.listShopcar();}

     @Override
     public int InsertShopcar(Shopcar shopcar){
         return shopcardao.InsertShopcar(shopcar);
     }

    //返回购物车表里的信息，根据用户ID
    @Override
    public List<ShopCarDto> selectGoodsByUserId(int userId) {
        return shopcardao.selectGoodsByUserId(userId);
    }

    //修改购物车
    @Override
    public int updateShopCarByUserIdAndGoodsId(Shopcar shopcar) {
        return shopcardao.updateShopCarByUserIdAndGoodsId(shopcar);
    }

    //删除表中的数据
    @Override
    public int deleteShopCarByUserIdAndGoodsId(int id) {
        return shopcardao.deleteShopCar(id);
    }


    //删除表中的数据
    @Override
    public int deleteShopCar(int id) {
        return shopcardao.deleteShopCar(id);
    }

    @Override
    public int delshopcar() {
        return shopcardao.delshopcar();
    }

    @Override
    public int updatestuta(int id) {
        return shopcardao.updatestuta(id);
    }

    @Override
    public int upstuta(int id) {
        return shopcardao.upstuta(id);
    }

    @Override
    public int setstuta(int id) {
        return shopcardao.setstuta(id);
    }

    @Override
    public int updatenum(int id,int num) {
        return shopcardao.updatenum(id,num);
    }
}

