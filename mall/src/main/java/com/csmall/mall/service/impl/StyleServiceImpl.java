package com.csmall.mall.service.impl;

import com.csmall.mall.dao.StyleDao;
import com.csmall.mall.model.Style;
import com.csmall.mall.service.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class StyleServiceImpl implements StyleService {

    @Autowired
    private StyleDao styleDao;


    @Override
    public List<Style> listSytle(int goodId) {
        return styleDao.listStyle(goodId);
    }
}
