package com.csmall.mall.service.impl;

import com.csmall.mall.base.Results;
import com.csmall.mall.dao.UserDao;
import com.csmall.mall.deo.UserDeo;
import com.csmall.mall.model.User;
import com.csmall.mall.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserDeo userDeo;


    @Override
    public List<User> listUser() {
        return userDao.findAll();
    }



    @Override
    public User getUserByusername(String username) {
        return userDao.findByUsername(username);
    }

    @Override
    public boolean isExist(String username) {
        User user = getUserByusername(username);
        return user != null;
    }

    @Override
    public void addUser(User user) {
        userDao.save(user);
    }

    @Override
    public User find(String username, String password) {
        return userDao.findAllByUsernameAndPassword(username,password);
    }

    @Override
    public Results<User> list(Integer startPage,Integer limits) {
        return Results.success(userDeo.countUser().intValue(),userDeo.list(startPage,limits));
    }

    @Override
    public User editUserById(Integer userid) {
        return userDeo.getUserByUserId(userid);
    }

    @Override
    public int updateUser(User user) {
        return userDeo.updateUser(user);
    }

    @Override
    public int addUserByLayui(User user) {
        return userDeo.addUserByLayui(user);
    }

    @Override
    public Results<User> findUserByUsername(String username, Integer startPage, Integer limits) {
        return Results.success(userDeo.findUser(username).intValue(),userDeo.findUserByUsername(username,startPage,limits));
    }

    @Override
    public int deleteUserByUserid(Integer userid) {
        return userDeo.deleteUserByUserId(userid);
    }

    @Override
    public User getUserByname(String username) {
        return userDeo.getUserByname(username);
    }



  /*  @Override
    public User add(User user) {
        return userDao.add(user);
    }*/



}
