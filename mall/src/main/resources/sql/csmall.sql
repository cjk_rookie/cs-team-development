/*
 Navicat Premium Data Transfer

 Source Server         : csmall
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : csmall

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 08/12/2020 15:11:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address`  (
  `addressid` int(0) NOT NULL AUTO_INCREMENT,
  `userid` int(0) NOT NULL,
  `consignee` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人',
  `aphone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '电话',
  `province` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省',
  `city` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '市',
  `county` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '县',
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '详细地址',
  PRIMARY KEY (`addressid`) USING BTREE,
  INDEX `fk_address_u`(`userid`) USING BTREE,
  CONSTRAINT `fk_address_u` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of address
-- ----------------------------
INSERT INTO `address` VALUES (1, 1, '张三', '15666666666', '广东省', '珠海市', '金湾区', '城职');
INSERT INTO `address` VALUES (2, 2, 'cs', '18312203861', '广东省', '珠海市', '金湾区', '科干');

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `adminid` int(0) NOT NULL,
  `name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`adminid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'admin', 'admin', NULL);

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `categoriesid` int(0) NOT NULL AUTO_INCREMENT COMMENT '类别',
  `cname` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别名',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  PRIMARY KEY (`categoriesid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, '女装男装', NULL);
INSERT INTO `categories` VALUES (2, '鞋类箱包', NULL);
INSERT INTO `categories` VALUES (3, '母婴用品', NULL);
INSERT INTO `categories` VALUES (4, '护肤彩妆', NULL);
INSERT INTO `categories` VALUES (5, '零食', NULL);
INSERT INTO `categories` VALUES (6, '珠宝配饰', NULL);
INSERT INTO `categories` VALUES (7, '家装建材', NULL);
INSERT INTO `categories` VALUES (8, '家居家纺', NULL);
INSERT INTO `categories` VALUES (9, '百货市场', NULL);
INSERT INTO `categories` VALUES (10, '汽车·用品', NULL);
INSERT INTO `categories` VALUES (11, '手机数码', NULL);
INSERT INTO `categories` VALUES (12, '家电办公', NULL);
INSERT INTO `categories` VALUES (13, '户外运动', NULL);
INSERT INTO `categories` VALUES (14, '其他服务', NULL);

-- ----------------------------
-- Table structure for commentary
-- ----------------------------
DROP TABLE IF EXISTS `commentary`;
CREATE TABLE `commentary`  (
  `commentaryid` int(0) NOT NULL COMMENT '评论表',
  `userid` int(0) NULL DEFAULT NULL,
  `goodsid` int(0) NULL DEFAULT NULL,
  `rank` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评论等级',
  `time` datetime(0) NULL DEFAULT NULL COMMENT '评论时间',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评论内容',
  PRIMARY KEY (`commentaryid`) USING BTREE,
  INDEX `fk_comm_u`(`userid`) USING BTREE,
  INDEX `fk_comm_g`(`goodsid`) USING BTREE,
  CONSTRAINT `fk_comm_g` FOREIGN KEY (`goodsid`) REFERENCES `goods` (`goosid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_comm_u` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of commentary
-- ----------------------------
INSERT INTO `commentary` VALUES (1, 1, 1, NULL, NULL, '好');

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `goosid` int(0) NOT NULL AUTO_INCREMENT COMMENT '商品表',
  `goodsname` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `goodsprice` decimal(10, 2) NULL DEFAULT NULL COMMENT '单价',
  `discount` int(0) NULL DEFAULT NULL COMMENT '折扣',
  `sheiftime` datetime(0) NULL DEFAULT NULL COMMENT '上架时间',
  `gimages` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `categoriesid` int(0) NULL DEFAULT NULL COMMENT '类别',
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简介',
  `invertory` int(0) NULL DEFAULT NULL COMMENT '库存',
  PRIMARY KEY (`goosid`) USING BTREE,
  INDEX `categoriesid`(`categoriesid`) USING BTREE,
  CONSTRAINT `fk_goods_c` FOREIGN KEY (`categoriesid`) REFERENCES `categories` (`categoriesid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES (1, '男士2020新款卫衣', 230.00, NULL, '2020-01-01 00:00:00', '/static/image/衣服/Pasok0.10.jpg', 1, '(Pasok)潮嘻哈宽松韩版潮流连帽秋装潮牌学生衣服外套', 500);
INSERT INTO `goods` VALUES (2, '2020冬季新款男士棉衣', 350.00, NULL, '2019-11-05 08:00:00', '/static/image/衣服/佐卡曼1.JPG', 1, '(佐卡曼)潮流帅气冬装羽绒棉袄', 400);
INSERT INTO `goods` VALUES (3, '恒源祥秋季长袖t恤男', 168.00, NULL, '2020-09-17 08:00:00', '/static/image/衣服/恒源祥0.1.JPG', 1, '(恒源祥)翻领丝光棉中年宽松休闲打底polo衫潮流上衣', 600);
INSERT INTO `goods` VALUES (4, '恒源祥半高领打底毛衣女', 688.00, NULL, '2020-09-17 08:00:00', '/static/image/衣服/恒源祥1.1.JPG', 1, '(恒源祥)宽松外穿加厚保暖羊毛秋冬季软糯毛针织衫', 300);
INSERT INTO `goods` VALUES (5, '恒源祥轻薄羽绒服女中长款', 699.00, NULL, '2020-09-17 08:00:00', '/static/image/衣服/恒源祥（女0）1.JPG', 1, '(恒源祥)2020年新款女大码妈妈薄款女装冬装外套', 200);
INSERT INTO `goods` VALUES (6, '回力椰子男鞋2020新款运动鞋', 258.00, NULL, '2020-09-25 09:00:00', '/static/image/鞋类箱包/1.0.PNG', 2, '(回力)秋冬季鞋子男潮鞋百搭休闲男生跑步运动鞋', 500);
INSERT INTO `goods` VALUES (7, '回力男鞋子2020新款高帮运动板鞋', 218.00, NULL, '2020-09-25 09:00:00', '/static/image/鞋类箱包/2.0.jpg', 2, '(回力)aj联名空军一号高帮运动板鞋', 100);
INSERT INTO `goods` VALUES (8, '回力高帮帆布鞋', 299.00, NULL, '2020-09-25 09:00:00', '/static/image/鞋类箱包/3.0.jpg', 2, '(回力)英伦风新款休闲百搭软底潮流运动工装马丁靴', 200);
INSERT INTO `goods` VALUES (9, '回力手绘皮卡丘爆改联名帆布鞋', 230.00, NULL, '2020-09-25 09:00:00', '/static/image/鞋类箱包/4.0.jpg', 2, '(回力)国潮情侣高帮鸳鸯鞋PVC电尾女', 180);
INSERT INTO `goods` VALUES (10, '秋冬回力女鞋大码健步鞋', 138.00, NULL, '2020-09-25 09:00:00', '/static/image/鞋类箱包/5.0.jpg', 2, '(回力)秋冬回力女鞋大码健步鞋中老年妈妈鞋皮面软底防滑休闲运动跑步鞋\r\n(回力)秋冬回力女鞋大码健步鞋中老年妈妈鞋皮面软底防滑休闲运动跑步鞋', 260);
INSERT INTO `goods` VALUES (11, '贝亲玻璃奶瓶套装', 280.00, NULL, '2019-11-18 09:00:00', '/static/image/母婴用品/1.1.PNG', 3, '(贝亲)新生婴儿宽口径160+240ml送LL奶嘴喂养三件套', 500);
INSERT INTO `goods` VALUES (12, '婴儿双向推车', 600.00, NULL, '2019-11-18 09:00:00', '/static/image/母婴用品/0.1.png', 3, '(贝亲)可坐可躺简易轻便折叠高景观宝宝儿童新生手推伞车', 800);
INSERT INTO `goods` VALUES (13, 'Pigeon贝亲婴儿宽口玻璃奶瓶套装', 250.00, NULL, '2019-11-18 09:00:00', '/static/image/母婴用品/2.1.PNG', 3, '(贝亲)宽口玻璃奶瓶套装160ml+240ml新生儿母乳自然实感', 600);
INSERT INTO `goods` VALUES (14, '婴儿洗护润肤套装洗发沐浴二合一', 230.00, NULL, '2019-11-18 09:00:00', '/static/image/母婴用品/3.2.PNG', 3, '(贝亲)二合一护臀膏润肤露', 300);
INSERT INTO `goods` VALUES (15, '贝亲玻璃奶瓶PPSU配件套', 30.00, NULL, '2019-11-18 09:00:00', '/static/image/母婴用品/4.1.PNG', 3, '(贝亲)防摔硅胶护套耐摔奶瓶适配保护把手柄', 500);
INSERT INTO `goods` VALUES (16, '轻薄持妆粉底液', 199.00, NULL, '2018-12-23 00:00:00', '/static/image/护肤彩妆/粉底液/粉底液1.jpg', 4, '轻薄持妆粉底液', 800);
INSERT INTO `goods` VALUES (17, '醉美东方唇膏', 78.00, NULL, '2019-01-01 00:00:00', '/static/image/护肤彩妆/口红/口红1.jpg', 4, '醉美东方唇膏', 200);
INSERT INTO `goods` VALUES (18, '玫瑰润泽保湿舒缓面膜', 158.00, NULL, '2019-01-01 00:00:00', '/static/image/护肤彩妆/面膜/保湿面膜1.jpg', 4, '馥蕾诗玫瑰润泽保湿舒缓面膜', 200);
INSERT INTO `goods` VALUES (19, '毛孔净透深层洁肤霜', 139.00, NULL, '2019-01-01 00:00:00', '/static/image/护肤彩妆/洗面奶/洗面奶1.jpg', 4, '洗面奶', 500);
INSERT INTO `goods` VALUES (20, '出云温泉卸妆水', 30.00, NULL, '2019-12-08 00:00:00', '/static/image/护肤彩妆/卸妆水/卸妆水1.jpg', 4, '卸妆水', 800);
INSERT INTO `goods` VALUES (21, '碧根果', 25.00, NULL, '2020-11-01 00:00:00', '/static/image/零食/碧根果_1.jpg', 5, '三只松鼠', 900);
INSERT INTO `goods` VALUES (22, '核桃', 45.00, NULL, '2020-11-01 00:00:00', '/static/image/零食/核桃_1.jpg', 5, '三只松鼠', 1000);
INSERT INTO `goods` VALUES (23, '芒果干', 20.00, NULL, '2020-11-01 00:00:00', '/static/image/零食/芒果_1.jpg', 5, '三只松鼠', 800);
INSERT INTO `goods` VALUES (24, '蒸蛋糕', 30.00, NULL, '2020-11-01 00:00:00', '/static/image/零食/面包_1.jpg', 5, '三只松鼠', 1000);
INSERT INTO `goods` VALUES (25, '夏威夷果', 35.00, NULL, '2020-11-01 00:00:00', '/static/image/零食/夏威夷果_1.jpg', 5, '三只松鼠', 950);
INSERT INTO `goods` VALUES (26, '新上市5g全网通骁龙865游戏手机', 1588.00, NULL, '2020-05-19 00:00:00', '/static/image/手机数码/1.1.jpg', 11, '华为', 500);
INSERT INTO `goods` VALUES (27, '128G智能手机官方旗舰畅想10plus', 1189.00, NULL, '2020-05-19 00:00:00', '/static/image/手机数码/2.1.PNG', 11, '华为', 680);
INSERT INTO `goods` VALUES (28, '华为p40新品nova8现货保时捷华为手机', 8000.00, NULL, '2020-05-19 00:00:00', '/static/image/手机数码/3.1.PNG', 11, '华为', 800);
INSERT INTO `goods` VALUES (29, '华为 Mate 40 pro麒麟9000', 7129.00, NULL, '2020-05-19 00:00:00', '/static/image/手机数码/4.1.jpg', 11, '华为', 950);
INSERT INTO `goods` VALUES (30, '华为mateX折叠5G双屏手机', 16000.00, NULL, '2020-05-19 00:00:00', '/static/image/手机数码/5.1.jpg', 11, '华为', 600);

-- ----------------------------
-- Table structure for orderform
-- ----------------------------
DROP TABLE IF EXISTS `orderform`;
CREATE TABLE `orderform`  (
  `orderformid` int(0) NOT NULL COMMENT '订单id',
  `userid` int(0) NOT NULL COMMENT '用户id',
  `addressid` int(0) NOT NULL COMMENT '地址id',
  `goodsid` int(0) NOT NULL COMMENT '商品id',
  `goodsnum` int(0) NULL DEFAULT NULL COMMENT '商品数量',
  `total` int(0) NULL DEFAULT NULL COMMENT '总价',
  `stutas` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  `btime` datetime(0) NULL DEFAULT NULL COMMENT '下单时间',
  `otiem` datetime(0) NULL DEFAULT NULL COMMENT '到货时间',
  PRIMARY KEY (`orderformid`) USING BTREE,
  INDEX `fk_orde_u`(`userid`) USING BTREE,
  INDEX `fk_orde_a`(`addressid`) USING BTREE,
  INDEX `fk_orde_g`(`goodsid`) USING BTREE,
  CONSTRAINT `fk_orde_a` FOREIGN KEY (`addressid`) REFERENCES `address` (`addressid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_orde_g` FOREIGN KEY (`goodsid`) REFERENCES `goods` (`goosid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_orde_u` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orderform
-- ----------------------------
INSERT INTO `orderform` VALUES (1, 1, 1, 1, 1, 230, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for shopcar
-- ----------------------------
DROP TABLE IF EXISTS `shopcar`;
CREATE TABLE `shopcar`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '购物车',
  `userid` int(0) NULL DEFAULT NULL,
  `goodsid` int(0) NULL DEFAULT NULL,
  `num` int(0) NULL DEFAULT NULL COMMENT '商品数量',
  `color` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '颜色分类',
  `style` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '规格',
  `stuta` int(0) NULL DEFAULT 0 COMMENT '1(表示已支付），0大家都懂',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_shop_u`(`userid`) USING BTREE,
  INDEX `fk_shop_g`(`goodsid`) USING BTREE,
  CONSTRAINT `fk_shop_g` FOREIGN KEY (`goodsid`) REFERENCES `goods` (`goosid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_shop_u` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shopcar
-- ----------------------------
INSERT INTO `shopcar` VALUES (1, 1, 1, 1, 'red', 'L', 1);
INSERT INTO `shopcar` VALUES (2, 2, 1, 1, 'white', 'XL', 0);
INSERT INTO `shopcar` VALUES (3, 2, 2, 2, 'red', 'L', 0);
INSERT INTO `shopcar` VALUES (4, 2, 3, 3, 'red', 'X', 0);
INSERT INTO `shopcar` VALUES (5, 2, 6, 1, 'red', 'L', 0);
INSERT INTO `shopcar` VALUES (6, 2, 4, 1, 'red', 'L', 0);
INSERT INTO `shopcar` VALUES (7, 2, 5, 1, 'red', 'L', 0);
INSERT INTO `shopcar` VALUES (9, 1, 2, 1, '蓝色', 'S', 0);
INSERT INTO `shopcar` VALUES (10, 1, 1, 1, '蓝色', 'XL', 0);

-- ----------------------------
-- Table structure for shops
-- ----------------------------
DROP TABLE IF EXISTS `shops`;
CREATE TABLE `shops`  (
  `shopsid` int(0) NOT NULL AUTO_INCREMENT COMMENT '商铺表',
  `shopsname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商铺名',
  `simages` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `goodcomments` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '好评率',
  `saddress` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `fansnum` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '粉丝数',
  `goodid` int(0) NULL DEFAULT NULL COMMENT '商品Id',
  PRIMARY KEY (`shopsid`) USING BTREE,
  INDEX `fk_shops_g`(`goodid`) USING BTREE,
  CONSTRAINT `fk_shops_g` FOREIGN KEY (`goodid`) REFERENCES `goods` (`goosid`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shops
-- ----------------------------
INSERT INTO `shops` VALUES (1, '康师傅官方旗舰店', NULL, '100%', '上海', '90万', NULL);
INSERT INTO `shops` VALUES (2, '小米官方旗舰店', NULL, '100%', '上海', '900万', NULL);
INSERT INTO `shops` VALUES (3, '华为官方旗舰店', NULL, '100%', '上海', '900万', NULL);
INSERT INTO `shops` VALUES (4, 'vivo官方旗舰店', NULL, '100%', '上海', '900万', NULL);
INSERT INTO `shops` VALUES (5, 'oppo官方旗舰店', NULL, '100%', '上海', '900万', NULL);
INSERT INTO `shops` VALUES (6, '美的官方旗舰店', NULL, '100%', '上海', '900万', NULL);
INSERT INTO `shops` VALUES (7, '格力官方旗舰店', NULL, '100%', '珠海', '900万', NULL);
INSERT INTO `shops` VALUES (8, '飞鹤官方旗舰店', NULL, '100%', '上海', '900万', NULL);
INSERT INTO `shops` VALUES (9, '雀巢官方旗舰店', NULL, '100%', '上海', '900万', NULL);
INSERT INTO `shops` VALUES (10, '三只老鼠官方旗舰店', NULL, '100%', '上海', '900万', NULL);
INSERT INTO `shops` VALUES (11, '卫龙官方旗舰店', NULL, '100%', '上海', '90万', NULL);
INSERT INTO `shops` VALUES (12, 'pasok官方旗舰店', NULL, '100%', '泉州', '100万', NULL);
INSERT INTO `shops` VALUES (13, '恒源祥官方旗舰店', NULL, '100%', '上海', '500万', NULL);
INSERT INTO `shops` VALUES (14, '佐卡曼旗舰店', NULL, '100%', '苏州', '300万', NULL);
INSERT INTO `shops` VALUES (15, '回力官方旗舰店', NULL, '100%', '上海', '700万', NULL);
INSERT INTO `shops` VALUES (16, '贝亲官方旗舰店', NULL, '100%', '广州', '300万', NULL);
INSERT INTO `shops` VALUES (17, '透蜜旗舰店', NULL, '100%', '广州', '400万', NULL);

-- ----------------------------
-- Table structure for style
-- ----------------------------
DROP TABLE IF EXISTS `style`;
CREATE TABLE `style`  (
  `sid` int(0) NOT NULL AUTO_INCREMENT,
  `goodId` int(0) NOT NULL COMMENT '商品ID',
  `style` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式',
  `color` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '颜色',
  PRIMARY KEY (`sid`) USING BTREE,
  INDEX `fk_sgoodsId`(`goodId`) USING BTREE,
  CONSTRAINT `fk_sgoodsId` FOREIGN KEY (`goodId`) REFERENCES `goods` (`goosid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of style
-- ----------------------------
INSERT INTO `style` VALUES (1, 1, 'S', '红色');
INSERT INTO `style` VALUES (2, 1, 'M', '蓝色');
INSERT INTO `style` VALUES (3, 1, 'X', '绿色');
INSERT INTO `style` VALUES (4, 1, 'XL', '黑色');
INSERT INTO `style` VALUES (5, 1, 'XXL', '白色');
INSERT INTO `style` VALUES (6, 2, 'S', '红色');
INSERT INTO `style` VALUES (7, 2, 'M', '蓝色');
INSERT INTO `style` VALUES (8, 2, 'X', '绿色');
INSERT INTO `style` VALUES (9, 2, 'XL', '黑色');
INSERT INTO `style` VALUES (10, 2, 'XXL', '白色');
INSERT INTO `style` VALUES (11, 3, 'S', '红色');
INSERT INTO `style` VALUES (12, 3, 'M', '白色');
INSERT INTO `style` VALUES (13, 3, 'X', '黑色');
INSERT INTO `style` VALUES (14, 3, 'XL', '黄色');
INSERT INTO `style` VALUES (15, 3, 'XXL', '紫色');
INSERT INTO `style` VALUES (16, 4, 'S', '红色');
INSERT INTO `style` VALUES (17, 4, 'M', '军绿色');
INSERT INTO `style` VALUES (18, 4, 'X', '白色');
INSERT INTO `style` VALUES (19, 4, 'XL', '黑白色');
INSERT INTO `style` VALUES (20, 4, 'XXL', '橙色');
INSERT INTO `style` VALUES (21, 5, 'S', '红色');
INSERT INTO `style` VALUES (22, 5, 'M', '深蓝色');
INSERT INTO `style` VALUES (23, 5, 'X', '白色');
INSERT INTO `style` VALUES (24, 5, 'XL', '黄色');
INSERT INTO `style` VALUES (25, 5, 'XXL', '紫色');
INSERT INTO `style` VALUES (26, 6, '38', '红色');
INSERT INTO `style` VALUES (27, 6, '39', '深蓝色');
INSERT INTO `style` VALUES (28, 6, '40', '白色');
INSERT INTO `style` VALUES (29, 6, '41', '黄色');
INSERT INTO `style` VALUES (30, 6, '42', '黑白色');
INSERT INTO `style` VALUES (31, 6, '43', '黑红色');
INSERT INTO `style` VALUES (32, 6, '44', '黑蓝色');
INSERT INTO `style` VALUES (33, 7, '38', '红色');
INSERT INTO `style` VALUES (34, 7, '39', '深蓝色');
INSERT INTO `style` VALUES (35, 7, '40', '白色');
INSERT INTO `style` VALUES (36, 7, '41', '黄色');
INSERT INTO `style` VALUES (37, 7, '42', '黑白色');
INSERT INTO `style` VALUES (38, 7, '43', '黑红色');
INSERT INTO `style` VALUES (39, 7, '44', '黑蓝色');
INSERT INTO `style` VALUES (40, 8, '38', '红色');
INSERT INTO `style` VALUES (41, 8, '39', '深蓝色');
INSERT INTO `style` VALUES (42, 8, '40', '白色');
INSERT INTO `style` VALUES (43, 8, '41', '黄色');
INSERT INTO `style` VALUES (44, 8, '42', '黑白色');
INSERT INTO `style` VALUES (45, 8, '43', '黑红色');
INSERT INTO `style` VALUES (46, 8, '44', '黑蓝色');
INSERT INTO `style` VALUES (47, 9, '38', '红色');
INSERT INTO `style` VALUES (48, 9, '39', '深蓝色');
INSERT INTO `style` VALUES (49, 9, '40', '白色');
INSERT INTO `style` VALUES (50, 9, '41', '黄色');
INSERT INTO `style` VALUES (51, 9, '42', '黑白色');
INSERT INTO `style` VALUES (52, 9, '43', '黑红色');
INSERT INTO `style` VALUES (53, 9, '44', '黑蓝色');
INSERT INTO `style` VALUES (54, 10, '38', '红色');
INSERT INTO `style` VALUES (55, 10, '39', '深蓝色');
INSERT INTO `style` VALUES (56, 10, '40', '白色');
INSERT INTO `style` VALUES (57, 10, '41', '黄色');
INSERT INTO `style` VALUES (58, 10, '42', '黑白色');
INSERT INTO `style` VALUES (59, 10, '43', '黑红色');
INSERT INTO `style` VALUES (60, 10, '44', '黑蓝色');
INSERT INTO `style` VALUES (61, 11, '150ml+250ml', '红色');
INSERT INTO `style` VALUES (62, 11, '250ml+250ml', '深蓝色');
INSERT INTO `style` VALUES (63, 11, '150ml', '白色');
INSERT INTO `style` VALUES (64, 11, '250ml', '黑蓝色');
INSERT INTO `style` VALUES (65, 12, '超强避震 宝宝才能更舒适', '红色');
INSERT INTO `style` VALUES (66, 12, '品质好货 坚持做好品质推车', '藏青色');
INSERT INTO `style` VALUES (67, 12, '', '白色');
INSERT INTO `style` VALUES (68, 12, '', '蓝色');
INSERT INTO `style` VALUES (69, 13, '150ml', '红色');
INSERT INTO `style` VALUES (70, 13, '250ml', '藏青色');
INSERT INTO `style` VALUES (71, 13, '150ml+250ml', '白色');
INSERT INTO `style` VALUES (72, 13, '250ml+250ml', '蓝色');
INSERT INTO `style` VALUES (73, 14, '350ml', '洗发沐浴合一');
INSERT INTO `style` VALUES (74, 15, '底座', '蓝色');
INSERT INTO `style` VALUES (75, 15, '把手', '绿色');
INSERT INTO `style` VALUES (76, 15, '把手+底座', '红色');
INSERT INTO `style` VALUES (77, 15, '把手+底座+学饮杯吸', '黄色');
INSERT INTO `style` VALUES (78, 16, NULL, 'M01黄调自然色（水润遮瑕款）');
INSERT INTO `style` VALUES (79, 16, NULL, 'M02黄调亮肤色（水润遮瑕款）');
INSERT INTO `style` VALUES (80, 16, NULL, 'M02黄调象牙色（水润遮瑕款）');
INSERT INTO `style` VALUES (81, 16, NULL, 'P01黄调自然色（哑光遮瑕款）');
INSERT INTO `style` VALUES (82, 16, NULL, 'Y02黄调亮肤色（哑光遮瑕款）');
INSERT INTO `style` VALUES (83, 17, NULL, '巫山红锈-小辣椒色');
INSERT INTO `style` VALUES (84, 17, NULL, '蓝田墨玉-厘子色');
INSERT INTO `style` VALUES (85, 17, NULL, '栖霞晚枫-红橘色');
INSERT INTO `style` VALUES (86, 17, NULL, '长安胭脂-正红色');
INSERT INTO `style` VALUES (87, 17, NULL, '韶关丹霞-暖调红棕色');
INSERT INTO `style` VALUES (88, 18, '30ml', NULL);
INSERT INTO `style` VALUES (89, 18, '100ml', NULL);
INSERT INTO `style` VALUES (90, 19, '120g', '白色');
INSERT INTO `style` VALUES (91, 20, '300g/ml', NULL);
INSERT INTO `style` VALUES (92, 21, '250G*2罐(下单即送零食)', '【奶油味 特惠装】');
INSERT INTO `style` VALUES (93, 21, '250G*2罐(下单即送零食)', '【草本味 特惠装');
INSERT INTO `style` VALUES (94, 21, '250G*2罐(下单即送零食)', '【椒盐味 大颗粒】');
INSERT INTO `style` VALUES (95, 21, '250G*2罐(买1斤送半斤)', '【奶油味 特惠装】');
INSERT INTO `style` VALUES (96, 22, '500g', '原色奶香味');
INSERT INTO `style` VALUES (97, 22, '500g', '草本味');
INSERT INTO `style` VALUES (98, 22, '500g', '椒盐味');
INSERT INTO `style` VALUES (99, 22, '500g', '纸皮核桃');
INSERT INTO `style` VALUES (100, 23, NULL, '芒果干510g（厚切大片 每片独立包装）');
INSERT INTO `style` VALUES (101, 23, NULL, '芒果干510g*2袋');
INSERT INTO `style` VALUES (102, 24, '900g', '奶香');
INSERT INTO `style` VALUES (103, 24, '905g', '蓝莓');
INSERT INTO `style` VALUES (104, 24, '650g', '芝士');
INSERT INTO `style` VALUES (105, 25, '2斤', '奶油味');
INSERT INTO `style` VALUES (106, 25, '3斤', '原味');
INSERT INTO `style` VALUES (107, 25, '5斤', '奶油味【大粒】');
INSERT INTO `style` VALUES (108, 26, '32G', '红色');
INSERT INTO `style` VALUES (109, 26, '64G', '白色');
INSERT INTO `style` VALUES (110, 26, '128G', '黄色');
INSERT INTO `style` VALUES (111, 26, '256G', '黑色');
INSERT INTO `style` VALUES (112, 27, '32G', '红色');
INSERT INTO `style` VALUES (113, 27, '64G', '白色');
INSERT INTO `style` VALUES (114, 27, '128G', '黄色');
INSERT INTO `style` VALUES (115, 27, '256G', '黑色');
INSERT INTO `style` VALUES (116, 28, '32G', '红色');
INSERT INTO `style` VALUES (117, 28, '64G', '白色');
INSERT INTO `style` VALUES (118, 28, '128G', '黄色');
INSERT INTO `style` VALUES (119, 28, '256G', '黑色');
INSERT INTO `style` VALUES (120, 29, '32G', '红色');
INSERT INTO `style` VALUES (121, 29, '64G', '白色');
INSERT INTO `style` VALUES (122, 29, '128G', '黄色');
INSERT INTO `style` VALUES (123, 29, '256G', '黑色');
INSERT INTO `style` VALUES (124, 30, '32G', '红色');
INSERT INTO `style` VALUES (125, 30, '64G', '白色');
INSERT INTO `style` VALUES (126, 30, '128G', '黄色');
INSERT INTO `style` VALUES (127, 30, '256G', '黑色');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `userid` int(0) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `uimage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `sex` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  PRIMARY KEY (`userid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '张三', '123456', '15666666666', NULL, NULL, NULL);
INSERT INTO `user` VALUES (2, 'cs', '123456', '18312203861', NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
